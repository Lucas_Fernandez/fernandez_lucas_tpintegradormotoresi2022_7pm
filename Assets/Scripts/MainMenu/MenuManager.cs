using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    public Button startButton;

    public Slider volumeControl;

    private bool gameStarted;

    private float timeLeft;

    public Animator loadingFade;

    public float loadingTime;

    void Start()
    {
        gameStarted = false;
        AudioManager.instance.PlaySound("MainMenuTheme");
    }

    private void Update()
    {
        if (
            Input.GetKey(KeyCode.W) ||
            Input.GetKey(KeyCode.A) ||
            Input.GetKey(KeyCode.S) ||
            Input.GetKey(KeyCode.D)
        )
        {
            AudioManager.instance.PlaySound("SelectionSound");
        }

        if (startButton != null)
        {
            PlayButtonController script =
                (PlayButtonController)
                startButton.GetComponent(typeof (PlayButtonController));

            if (script != null && !gameStarted)
            {
                if (script.clicked())
                {
                    gameStarted = true;
                    Play();
                    AudioManager.instance.StopSound("MainMenuTheme");
                    AudioManager.instance.PlaySound("Fanfare");
                }
            }
        }

        controlVolume();
    }

    private void Play()
    {
        loadingFade.SetTrigger("Start");
        StartCoroutine(startGameIn(loadingTime));
    }

    public void Exit()
    {
        Debug.Log("Quit");
        Application.Quit();
    }

    public void controlVolume()
    {
        AudioManager
            .instance
            .VolumeControl("MainMenuTheme", volumeControl.value);
    }

    public void playSelected()
    {
        AudioManager.instance.PlaySound("Selected");
    }

    public IEnumerator startGameIn(float time)
    {
        timeLeft = time;
        while (timeLeft > -1)
        {
            yield return new WaitForSeconds(1.0f);
            timeLeft--;
        }
        SceneManager.LoadScene("SampleScene");
    }
}
