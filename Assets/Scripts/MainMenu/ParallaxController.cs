using UnityEngine;

public class ParallaxController : MonoBehaviour
{
    private Vector2 position;

    private Vector2 startPosition;

    public float moveModifier;

    private void Start()
    {
        startPosition = transform.position;
    }

    private void Update()
    {
        position = Camera.main.ScreenToViewportPoint(Input.mousePosition);
        float posX =
            Mathf
                .Lerp(transform.position.x,
                startPosition.x + (position.x * moveModifier),
                2f * Time.deltaTime);
        float posY =
            Mathf
                .Lerp(transform.position.y,
                startPosition.y + (position.y * moveModifier),
                2f * Time.deltaTime);
        transform.position = new Vector3(posX, posY, 0);
    }
}
