using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PlayButtonController : MonoBehaviour
{
    private bool pressedOnce;

    private bool wasClicked;

    private void Start()
    {
        pressedOnce = false;
        wasClicked = false;
    }

    // Update is called once per frame
    private void Update()
    {
        if (
            (
            Input.GetKey(KeyCode.W) ||
            Input.GetKey(KeyCode.A) ||
            Input.GetKey(KeyCode.S) ||
            Input.GetKey(KeyCode.D)
            ) &&
            !pressedOnce
        )
        {
            EventSystem.current.SetSelectedGameObject(this.gameObject);
            pressedOnce = true;
        }
    }

    public void TaskOnClick()
    {
        wasClicked = true;
    }

    public bool clicked()
    {
        return wasClicked;
    }
}
