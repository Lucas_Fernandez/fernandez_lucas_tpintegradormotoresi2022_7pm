using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitButtonController : MonoBehaviour
{
    private bool wasClicked;

    void Start()
    {
        wasClicked = false;
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void TaskOnClick()
    {
        wasClicked = true;
    }

    public bool clicked()
    {
        return wasClicked;
    }
}
