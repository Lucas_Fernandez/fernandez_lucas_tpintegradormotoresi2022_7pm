using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    private TMPro.TMP_Text messageText;

    public TextWriter textWriter;

    public string textToWrite;

    private void Awake()
    {
        messageText =
            transform
                .Find("Text")
                .Find("Message")
                .GetComponent<TMPro.TMP_Text>();
    }

    void Start()
    {
    }

    void Update()
    {
    }

    public void Write (string text){
        textWriter.AddWriter(messageText, text, .05f, true);
    }
}
