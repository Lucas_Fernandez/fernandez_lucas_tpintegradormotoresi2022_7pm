using UnityEngine;

[System.Serializable]
public class Sound
{
    public string Name;

    public AudioClip SoundClip;

    [Range(0f, 1f)]
    public float Volume;

    [Range(0f, 1f)]
    public float pitch;

    public bool repeat;

    [HideInInspector]
    public AudioSource AudioSource;
}
