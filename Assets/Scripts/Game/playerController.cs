using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class playerController : MonoBehaviour
{
    private Rigidbody rb;

    Vector3 movement;

    public CapsuleCollider col;

    public float movementSpeed = 0.75f;

    public float loadingTime;

    private float timeLeft;

    private Animator animator;

    public Animator loadingFade;

    public bool isWalking;

    public bool isRunning;

    public string currentLocation;

    public Camera rayCastCamera;

    public Canvas controls;

    public string currentlyFacing;

    public string playerName;

    private bool talkedToProff;

    public Pokemon myPokemon;

    public playerData playerData;

    void Start()
    {
        //Setters
        playerName = "Player";
        playerData.setName (playerName);
        isWalking = false;
        isRunning = false;
        talkedToProff = false;
        currentLocation = null;
        myPokemon = null;

        //Getters
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
        col = GetComponent<CapsuleCollider>();
    }

    void Update()
    {
        baseControllers();
        actions();
    }

    void FixedUpdate()
    {
        //Moving the player
        rb
            .MovePosition(rb.position +
            movement * movementSpeed * Time.fixedDeltaTime);

        rayCast();
    }

    private void rayCast()
    {
        //Raycasting
        Ray ray =
            rayCastCamera.ViewportPointToRay(new Vector3(0.25f, 0.25f, 0));
        RaycastHit hit;

        //Display hitted object
        if ((Physics.Raycast(ray, out hit) == true) && hit.distance < 0.25f)
        {
            //Check Objects
            if (
                hit.collider.name == "T.V." ||
                hit.collider.name == "P.C." ||
                hit.collider.name == "WaterBall" ||
                hit.collider.name == "FireBall" ||
                hit.collider.name == "GrassBall"
            )
            {
                currentlyFacing = hit.collider.name;

                controls.transform.Find("Check").GetComponent<Image>().enabled =
                    true;
            }

            //Talk
            if (
                hit.collider.name == "PlayerMom" ||
                hit.collider.name == "ProffesorPokemon" ||
                hit.collider.name == "Grandson(Clone)"
            )
            {
                currentlyFacing = hit.collider.name;
                controls.transform.Find("Talk").GetComponent<Image>().enabled =
                    true;
            }
        }
        else
        {
            currentlyFacing = null;
            controls.transform.Find("Check").GetComponent<Image>().enabled =
                false;
            controls.transform.Find("Talk").GetComponent<Image>().enabled =
                false;
            controls.transform.Find("Next").GetComponent<Image>().enabled =
                false;
            controls
                .transform
                .Find("PokeSprites")
                .transform
                .Find("Grass")
                .GetComponent<Image>()
                .enabled = false;
            controls
                .transform
                .Find("PokeSprites")
                .transform
                .Find("Water")
                .GetComponent<Image>()
                .enabled = false;
            controls
                .transform
                .Find("PokeSprites")
                .transform
                .Find("Fire")
                .GetComponent<Image>()
                .enabled = false;
            controls
                .transform
                .Find("ChooseButton")
                .GetComponent<Image>()
                .enabled = false;
            controls
                .transform
                .Find("ChooseButton")
                .transform
                .Find("ChooseText")
                .GetComponent<TMPro.TMP_Text>()
                .enabled = false;
            TextUI(false);
        }
    }

    private void actions()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (currentlyFacing == "T.V.")
            {
                TextUI(true);
                controls
                    .transform
                    .GetComponent<UIManager>()
                    .Write("There's a TV here...");
            }

            if (currentlyFacing == "P.C.")
            {
                TextUI(true);
                controls
                    .transform
                    .GetComponent<UIManager>()
                    .Write(playerName +
                    " booted up the PC and checked the Mailbox!");
            }

            if (currentlyFacing == "PlayerMom")
            {
                TextUI(true);
                controls
                    .transform
                    .GetComponent<UIManager>()
                    .Write(playerName +
                    "! 'Supp?, the proffesor was looking for you");
            }
            if (currentlyFacing == "ProffesorPokemon")
            {
                if (myPokemon == null)
                {
                    TextUI(true);
                    controls
                        .transform
                        .GetComponent<UIManager>()
                        .Write(playerName +
                        "! I'll give you a Pokemon for free if you fight my grandson, go ahead");
                    talkedToProff = true;
                }
                else
                {
                    TextUI(true);
                    controls
                        .transform
                        .GetComponent<UIManager>()
                        .Write(playerName +
                        "! Great Choice! My grandson is outside");
                }
            }
            if (currentlyFacing == "Grandson(Clone)")
            {
                TextUI(true);
                controls
                    .transform
                    .GetComponent<UIManager>()
                    .Write("I'm the grandson, prepare to battle!");
                loadingFade.SetTrigger("Start");
                StartCoroutine(nextScene(loadingTime));
            }
            if (currentlyFacing == "GrassBall")
            {
                TextUI(true);
                if (!talkedToProff)
                {
                    controls
                        .transform
                        .GetComponent<UIManager>()
                        .Write("That's a PokeBall!");
                }
                else
                {
                    controls
                        .transform
                        .Find("PokeSprites")
                        .transform
                        .Find("Grass")
                        .GetComponent<Image>()
                        .enabled = true;
                    controls
                        .transform
                        .GetComponent<UIManager>()
                        .Write("That's Treecko! The Grass Pokemon!");
                    controls
                        .transform
                        .Find("ChooseButton")
                        .GetComponent<Image>()
                        .enabled = true;
                    controls
                        .transform
                        .Find("ChooseButton")
                        .transform
                        .Find("ChooseText")
                        .GetComponent<TMPro.TMP_Text>()
                        .enabled = true;
                }
            }
            if (currentlyFacing == "WaterBall")
            {
                TextUI(true);
                if (!talkedToProff)
                {
                    controls
                        .transform
                        .GetComponent<UIManager>()
                        .Write("That's a PokeBall!");
                }
                else
                {
                    controls
                        .transform
                        .Find("PokeSprites")
                        .transform
                        .Find("Water")
                        .GetComponent<Image>()
                        .enabled = true;
                    controls
                        .transform
                        .GetComponent<UIManager>()
                        .Write("That's Totodile! The Water Pokemon!");
                    controls
                        .transform
                        .Find("ChooseButton")
                        .GetComponent<Image>()
                        .enabled = true;
                    controls
                        .transform
                        .Find("ChooseButton")
                        .transform
                        .Find("ChooseText")
                        .GetComponent<TMPro.TMP_Text>()
                        .enabled = true;
                }
            }
            if (currentlyFacing == "FireBall")
            {
                TextUI(true);
                if (!talkedToProff)
                {
                    controls
                        .transform
                        .GetComponent<UIManager>()
                        .Write("That's a PokeBall!");
                }
                else
                {
                    controls
                        .transform
                        .Find("PokeSprites")
                        .transform
                        .Find("Fire")
                        .GetComponent<Image>()
                        .enabled = true;
                    controls
                        .transform
                        .GetComponent<UIManager>()
                        .Write("That's Charmander! The Fire Pokemon!");
                    controls
                        .transform
                        .Find("ChooseButton")
                        .GetComponent<Image>()
                        .enabled = true;
                    controls
                        .transform
                        .Find("ChooseButton")
                        .transform
                        .Find("ChooseText")
                        .GetComponent<TMPro.TMP_Text>()
                        .enabled = true;
                }
            }
        }
    }

    private void TextUI(bool enabled)
    {
        controls
            .transform
            .Find("Text")
            .transform
            .Find("Background")
            .GetComponent<Image>()
            .enabled = enabled;
        controls
            .transform
            .Find("Text")
            .transform
            .Find("Message")
            .GetComponent<TMPro.TMP_Text>()
            .enabled = enabled;
    }

    private void baseControllers()
    {
        //Getting movement inputs
        movement.x = -Input.GetAxisRaw("Horizontal");
        movement.z = -Input.GetAxisRaw("Vertical");

        //Setting animator variables
        if (
            !(
            Input.GetKey(KeyCode.W) ||
            Input.GetKey(KeyCode.A) ||
            Input.GetKey(KeyCode.S) ||
            Input.GetKey(KeyCode.D)
            ) //If none of the movement keys is pressed the speedAnimation variable is not affected
        )
        {
            isWalking = false;
            isRunning = false;
            animator.SetFloat("Speed", movement.sqrMagnitude);
        }
        else if (
            Input.GetKey(KeyCode.LeftShift) //If Shift is pressed/released, movementSpeed increases/decreases and sets the speedAnimation variable
        )
        {
            isRunning = true;
            isWalking = false;
            movementSpeed = 1.75f;
            animator.SetFloat("Speed", movementSpeed);
        }
        else
        {
            isRunning = false;
            isWalking = true;
            movementSpeed = 0.75f;
            animator.SetFloat("Speed", movementSpeed);
        }
        rotationController();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Map"))
        {
            currentLocation = other.name;
        }
        if (other.gameObject.CompareTag("Teleporter"))
        {
            if (other != null)
            {
                placeToTp script =
                    (placeToTp) other.GetComponent(typeof (placeToTp));

                if (script != null)
                {
                    transform.position = script.newPlayerPosition;
                }
            }
        }
    }

    private void rotationController()
    {
        //Controls the rotation of the player depending on pressed keys
        if (Input.GetKey(KeyCode.W))
        {
            transform.localRotation = Quaternion.Euler(0, 180, 0);
        }

        if (Input.GetKey(KeyCode.S))
        {
            transform.localRotation = Quaternion.Euler(0, 0, 0);
        }

        if (Input.GetKey(KeyCode.A))
        {
            transform.localRotation = Quaternion.Euler(0, 90, 0);
        }

        if (Input.GetKey(KeyCode.D))
        {
            transform.localRotation = Quaternion.Euler(0, -90, 0);
        }

        if (Input.GetKey(KeyCode.D) && Input.GetKey(KeyCode.W))
        {
            transform.localRotation = Quaternion.Euler(0, -135, 0);
        }
        if (Input.GetKey(KeyCode.D) && Input.GetKey(KeyCode.S))
        {
            transform.localRotation = Quaternion.Euler(0, -45, 0);
        }
        if (Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.W))
        {
            transform.localRotation = Quaternion.Euler(0, 135, 0);
        }
        if (Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.S))
        {
            transform.localRotation = Quaternion.Euler(0, 35, 0);
        }
    }

    public void getPokemon()
    {
        if (
            controls
                .transform
                .Find("PokeSprites")
                .transform
                .Find("Fire")
                .GetComponent<Image>()
                .enabled
        )
        {
            myPokemon = new Pokemon();
            myPokemon.constructor("Charmander", "Fire", 7, 3);
            playerData.setPokemon (myPokemon);
        }

        if (
            controls
                .transform
                .Find("PokeSprites")
                .transform
                .Find("Grass")
                .GetComponent<Image>()
                .enabled
        )
        {
            myPokemon = new Pokemon();
            myPokemon.constructor("Treecko", "Grass", 10, 2);
            playerData.setPokemon (myPokemon);
        }

        if (
            controls
                .transform
                .Find("PokeSprites")
                .transform
                .Find("Water")
                .GetComponent<Image>()
                .enabled
        )
        {
            myPokemon = new Pokemon();
            myPokemon.constructor("Totodile", "Water", 14, 1);
            playerData.setPokemon (myPokemon);
        }

        GameObject[] pokeballs;
        pokeballs = GameObject.FindGameObjectsWithTag("pokeball");

        foreach (GameObject ball in pokeballs)
        {
            Destroy (ball);
        }

        controls.transform.Find("ChooseButton").GetComponent<Image>().enabled =
            false;
        controls
            .transform
            .Find("ChooseButton")
            .transform
            .Find("ChooseText")
            .GetComponent<TMPro.TMP_Text>()
            .enabled = false;
    }

    public bool playerWalking()
    {
        return isWalking;
    }

    public bool playerRunning()
    {
        return isRunning;
    }

    public string playerIsIn()
    {
        return currentLocation;
    }

    public IEnumerator nextScene(float time)
    {
        timeLeft = time;
        while (timeLeft > -1)
        {
            yield return new WaitForSeconds(1.0f);
            timeLeft--;
        }
        SceneManager.LoadScene("FightScene");
    }
}
