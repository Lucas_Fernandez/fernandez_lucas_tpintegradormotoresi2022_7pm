using UnityEngine;

[System.Serializable]
public class Pokemon
{
    public string Name;

    public string Type;

    public int hp;

    public int dmg;

    public void constructor(string Name, string Type, int hp, int dmg)
    {
        this.Name = Name;
        this.Type = Type;
        this.hp = hp;
        this.dmg = dmg;
    }
}
