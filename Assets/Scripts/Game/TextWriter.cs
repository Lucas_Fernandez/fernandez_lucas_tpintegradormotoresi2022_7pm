using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextWriter : MonoBehaviour
{
    private TMPro.TMP_Text uiText;

    private string futureText;

    private int characterIndex;

    private float speed;

    private float timer;

    private bool invisibleCharacters;

    public void AddWriter(
        TMPro.TMP_Text uiText,
        string futureText,
        float speed,
        bool invisibleCharacters
    )
    {
        this.uiText = uiText;
        this.futureText = futureText;
        this.speed = speed;
        this.invisibleCharacters = invisibleCharacters;
        characterIndex = 0;
    }

    private void Update()
    {
        if (uiText != null)
        {
            timer -= Time.deltaTime;
            {
                while (timer <= 0f)
                {
                    timer += speed;
                    characterIndex++;
                    string text = futureText.Substring(0, characterIndex);
                    if (invisibleCharacters)
                    {
                        text +=
                            "<color=#00000000>" +
                            futureText.Substring(characterIndex) +
                            "</color>";
                    }
                    uiText.text = text;
                    if (characterIndex >= futureText.Length)
                    {
                        uiText = null;
                        return;
                    }
                }
            }
        }
    }
}
