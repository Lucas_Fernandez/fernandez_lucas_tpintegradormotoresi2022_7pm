using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gameManager : MonoBehaviour
{
    public GameObject player;

    public string themePlaying;

    public GameObject[] maps;

    public GameObject grandSon;

    private bool spawnedGrandSon;

    private void Start()
    {
        spawnedGrandSon = false;
        themePlaying = null;
    }

    void Update()
    {
        if (player != null)
        {
            playerController script =
                (playerController)
                player.GetComponent(typeof (playerController));

            if (script != null)
            {
                if (!script.playerWalking())
                {
                    AudioManager.instance.PlaySound("WalkSound");
                }
                if (!script.playerRunning())
                {
                    AudioManager.instance.PlaySound("RunSound");
                }
                if (
                    (
                    script.playerIsIn() == "New Bark Town" ||
                    script.playerIsIn() == "PlayerRoom" ||
                    script.playerIsIn() == "Laboratory"
                    ) &&
                    themePlaying != "New Bark Town"
                )
                {
                    themePlaying = "New Bark Town";
                    AudioManager.instance.PlaySound("NewBarkTownTheme");
                }
                
                if (script.myPokemon != null && !spawnedGrandSon)
                {
                    Instantiate(grandSon,
                    new Vector3(3.852f, -1.1920929e-07f, 0.0579999983f),
                    Quaternion.Euler(0, -90, 0));
                    spawnedGrandSon = true;
                }
            }
        }
    }
}
