using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : MonoBehaviour
{
    public int hours;

    public int minutes;

    private int dawn = 360;

    public GameObject sun;

    public GameObject moon;

    public GameObject player;

    private GameObject[] windowLights;

    Vector3 angle;

    private void Awake()
    {
        lightsOff(sun.GetComponent<Light>());

        lightsOff(moon.GetComponent<Light>());
    }

    void Start()
    {
        hours = System.DateTime.Now.Hour;
        minutes = System.DateTime.Now.Minute;
        angle = transform.localEulerAngles;
        windowLights = GameObject.FindGameObjectsWithTag("WindowLight");

        if (hours >= 19 && hours < 5)
        {
            foreach (GameObject light in windowLights)
            {
                lightsOn(light.transform.GetChild(0).GetComponent<Light>());
                MeshRendererHandler("On", light);
            }
        }

        if (hours >= 5 && hours < 19)
        {
            foreach (GameObject light in windowLights)
            {
                lightsOff(light.transform.GetChild(0).GetComponent<Light>());
                MeshRendererHandler("Off", light);
            }
        }
    }

    void Update()
    {
        hours = System.DateTime.Now.Hour;
        minutes = System.DateTime.Now.Minute;
        sun.transform.localEulerAngles =
            new Vector3(getSunRotation(hours, minutes), angle.y, angle.z);

        moon.transform.localEulerAngles =
            new Vector3(-getSunRotation(hours, minutes), angle.y, angle.z);

        if (player != null)
        {
            playerController script =
                (playerController)
                player.GetComponent(typeof (playerController));

            if (script != null)
            {
                if (script.playerIsIn() == "New Bark Town")
                {
                    if (hours >= 5 && hours < 19)
                    {
                        lightsOn(sun.GetComponent<Light>());
                        lightsOff(moon.GetComponent<Light>());
                    }
                    else
                    {
                        lightsOff(sun.GetComponent<Light>());
                        lightsOn(moon.GetComponent<Light>());
                    }
                }
                else
                {
                    lightsOff(sun.GetComponent<Light>());

                    lightsOff(moon.GetComponent<Light>());
                }
            }
        }

        foreach (GameObject light in windowLights)
        {
            manageLights(19,
            5,
            light.transform.GetChild(0).GetComponent<Light>());
        }

        manageLights(19, 5, moon.GetComponent<Light>());
        manageLights(5, 19, sun.GetComponent<Light>());
    }

    private float getSunRotation(int hours, int minutes)
    {
        float timeInMin = hours * 60 + minutes;
        float offset = timeInMin - dawn;
        float dgr = offset / 4;

        if (dgr >= 360f)
        {
            dgr -= 360f;
        }
        return dgr;
    }

    private void lightsOff(Light lightToTurnOff)
    {
        lightToTurnOff.enabled = false;
    }

    private void lightsOn(Light lightToTurnOn)
    {
        lightToTurnOn.enabled = true;
    }

    private void MeshRendererHandler(string onOrOff, GameObject gObj)
    {
        if (onOrOff == "On")
        {
            gObj.GetComponent<MeshRenderer>().enabled = true;
        }
        if (onOrOff == "Off")
        {
            gObj.GetComponent<MeshRenderer>().enabled = false;
        }
    }

    private void manageLights(int timeToTurnOn, int timeToTurnOff, Light light)
    {
        if (timeToTurnOn == hours)
        {
            lightsOn (light);
        }

        if (timeToTurnOff == hours)
        {
            lightsOff (light);
        }
    }

    private void manageMesh(
        int timeToTurnOn,
        int timeToTurnOff,
        GameObject gObj
    )
    {
        if (timeToTurnOn == hours)
        {
            MeshRendererHandler("On", gObj);
        }

        if (timeToTurnOff == hours)
        {
            MeshRendererHandler("Off", gObj);
        }
    }
}
