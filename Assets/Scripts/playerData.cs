using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerData : MonoBehaviour
{
    public static playerData instance;

    public string playerName;

    public Pokemon playerPokemon;

    public GameObject player;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy (gameObject);
        }
        DontDestroyOnLoad (gameObject);
    }

    public void setName(string name)
    {
        playerName = name;
    }

    public void setPokemon(Pokemon pokemon)
    {
        playerPokemon = pokemon;
    }
}
