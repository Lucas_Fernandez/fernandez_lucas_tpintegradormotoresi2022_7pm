using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FightManager : MonoBehaviour
{
    public Canvas controls;

    public playerData pData;

    private float timeLeft;

    public List<string> messages = new List<string>();

    public Animator animator;

    private int i;

    private GameObject img;

    public Sprite fire;

    public Sprite water;

    public Sprite grass;

    void Start()
    {
        img = GameObject.FindGameObjectsWithTag("PokemonSprite")[0];
        if (pData.playerPokemon.Type == "Fire")
        {
            img.transform.Find("Image").GetComponent<Image>().sprite = fire;
        }
        if (pData.playerPokemon.Type == "Water")
        {
            img.transform.Find("Image").GetComponent<Image>().sprite = water;
        }
        if (pData.playerPokemon.Type == "Grass")
        {
            img.transform.Find("Image").GetComponent<Image>().sprite = grass;
        }
        i = 0;
        AudioManager.instance.StopSound("NewBarkTownTheme");
        AudioManager.instance.StopSound("WalkSound");
        AudioManager.instance.StopSound("RunSound");
        AudioManager.instance.PlaySound("FightTheme");
        TextUI(true);
        messages.Add("Humongous Grandson wants to fight!");
        messages.Add(pData.playerName + " sent out" + pData.playerPokemon.Name);
        messages.Add("Here is " + pData.playerPokemon.Name + "!");
        messages.Add(pData.playerPokemon.Name + " attacked!");
        messages.Add("But is not very effective...");
        messages.Add("Grandson attacked...." + pData.playerName + "!");
        messages.Add("YOU DIED!");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && i < messages.Count)
        {
            AudioManager.instance.PlaySound("SelectionSound");
            textDelivery(messages[i]);
            i++;
        }
        else if (Input.GetKeyDown(KeyCode.E) && i == 7)
        {
            AudioManager.instance.StopSound("FightTheme");
            SceneManager.LoadScene("Menu");
        }
        Debug.Log (i);
        animator.SetFloat("Swap", i);
    }

    private void TextUI(bool enabled)
    {
        controls
            .transform
            .Find("Text")
            .transform
            .Find("Background")
            .GetComponent<Image>()
            .enabled = enabled;
        controls
            .transform
            .Find("Text")
            .transform
            .Find("Message")
            .GetComponent<TMPro.TMP_Text>()
            .enabled = enabled;
    }

    public void textDelivery(string message)
    {
        controls.transform.GetComponent<UIManager>().Write(message);
    }
}
